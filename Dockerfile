#stage 1
FROM node:16-alpine as node
WORKDIR /app
COPY . .
# RUN npm install
# RUN npm run build
#stage 2
FROM nginx:alpine
# Copy the buil project to nginx
COPY --from=node /app/dist/test-crud-employee/ /usr/share/nginx/html
# Copy the default nginx.conf provided by project
COPY --from=node /app/nginx.conf /etc/nginx/conf.d/default.conf
#RUN /usr/bin/nginx -s reload
