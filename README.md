# TestCrudEmployee

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 16.2.11.

## Demo

See [Demo](http://103.175.220.179:7092/) page. Navigate to `http://103.175.220.179:7092/`.

For access with.
Username: `test`.
Password: `test123`.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The application will automatically reload if you change any of the source files.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory.
