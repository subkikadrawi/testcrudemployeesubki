import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AngularToastifyModule, ToastService } from 'angular-toastify';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { AuthInterceptorService } from './service/auth-interceptor.service';
import { CookiesService } from './service/cookies.service';
import { DefaultLayoutComponent } from './containers';
import { LoginComponent } from './modules/auth/login/login.component';
import { AuthGuardService } from './service/auth-guard.service';

const APP_CONTAINERS = [DefaultLayoutComponent]

@NgModule({
  declarations: [
    AppComponent,
    ...APP_CONTAINERS,
    LoginComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    AngularToastifyModule,
    ReactiveFormsModule,
    FormsModule,
    HttpClientModule
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthInterceptorService,
      multi: true
    },
    ToastService,
    CookiesService,
    AuthGuardService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
