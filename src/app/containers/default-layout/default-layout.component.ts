import { Component, OnInit } from '@angular/core'
import { UserSession } from '../../modules/auth/login/login.model'
import { CookiesService } from 'src/app/service/cookies.service'
import { Router } from '@angular/router'

@Component({
  selector: 'app-dashboard',
  templateUrl: './default-layout.component.html'
})
export class DefaultLayoutComponent implements OnInit {
  public userSession!: UserSession

  constructor(private cookieService: CookiesService,
    private router: Router) {}

  ngOnInit() {
    this.userSession = JSON.parse(this.cookieService.getCookies('userSession'))
  }
  logout():void {
    this.cookieService.deleteAllCookies()
    this.navigateUrl()
  }
  navigateUrl(): void {
    this.router.navigate(['/login'])
  }
}
