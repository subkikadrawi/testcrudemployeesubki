import { Component, OnInit } from '@angular/core'
import { FormGroup, FormBuilder, Validators } from '@angular/forms'
import { Router, ActivatedRoute } from '@angular/router'
import { CookiesService } from 'src/app/service/cookies.service'
import { LoginForm } from './login.model'
import Swal from 'sweetalert2'

@Component({
  selector: 'app-dashboard',
  templateUrl: 'login.component.html'
})
export class LoginComponent implements OnInit {

  loading: boolean
  form: FormGroup
  isLoggedIn: boolean
  userSession: any
  constructor(
    private fb: FormBuilder,
    private router: Router,
    private activatedroute: ActivatedRoute,
    private cookieService: CookiesService
  ) {
    this.loading = false
    this.isLoggedIn = false

    this.form = this.fb.group({
      // Define your form controls with validators if needed
      username: ['', Validators.required],
      password: ['', Validators.required],
    });
  }

  ngOnInit(): void {
    const userSession = this.cookieService.getCookies('userSession')
    if (userSession) this.navigateUrl()
  }

  onSubmit() {
    // Your form submission logic goes here
    if (this.form.valid) {
      // Form is valid, proceed with submission
      this.userSession = {
        userName: "test",
        password: "test123"
      }

      if (this.form.value.userName !== 'test' && this.form.value.password !== 'test123') {
        Swal.fire({
          title: "Invalid Access!",
          text: `Your username and password is invalid.`,
          icon: "error"
        })
        return
      }
      this.setAuthCookies()
      this.isLoggedIn = true
      this.navigateUrl()
      Swal.fire({
        title: "Wellcome!",
        icon: "success"
      })
    } else {
      console.log('Form is invalid. Please check the fields.');
      this.form.markAllAsTouched()
    }
  }

  setAuthCookies(): void {
    this.cookieService.setCookies('userSession', JSON.stringify(this.userSession))
  }

  navigateUrl(): void {
    this.router.navigate(['/'])
  }
}
