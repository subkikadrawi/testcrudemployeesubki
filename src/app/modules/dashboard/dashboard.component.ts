import { Component, OnInit } from '@angular/core'
import { CookiesService } from 'src/app/service/cookies.service'
import { Router } from '@angular/router'
import Swal from 'sweetalert2'
import { EmployeeService } from '../employee/employee.service'

@Component({
  templateUrl: 'dashboard.component.html'
})
export class DashboardComponent implements OnInit {
  titlePage = 'Dashboard'
  filterData: {
    inputSearch: string,
    currentPage: number,
    previousPage: number,
    isMaxPage: boolean,
    currentLimit: number,
    orderby: string,
    order: string
  }  = {
    inputSearch: '',
    currentPage: 1,
    previousPage: 1,
    isMaxPage: false,
    currentLimit: 20,
    orderby: 'id',
    order: 'asc'
  }
  listEmployee: any = []

  constructor(
    private employeeService: EmployeeService,
    private cookieService: CookiesService,
    private router: Router
  ) {
  }

  ngOnInit() {
    var filterData = this.cookieService.getCookies('filterEmployee')
    if (filterData) {
      this.filterData = JSON.parse(filterData)
    }
    this.getListEmployee()
  }

  logout():void {
    this.cookieService.deleteAllCookies()
    this.navigateUrl('/login')
  }
  navigateUrl(directTo: string): void {
    this.router.navigate([directTo])
  }

  handleSearch(event: any): void {
    this.cookieService.setCookies('filterEmployee',  JSON.stringify(this.filterData))
    this.getListEmployee()
  }

  handleNextPage(): void {
    this.filterData['currentPage'] = this.filterData.currentPage + 1
    this.cookieService.setCookies('filterEmployee',  JSON.stringify(this.filterData))
    this.getListEmployee()
  }

  handlePrevPage(): void {
    this.filterData['currentPage'] = this.filterData.currentPage - 1
    this.filterData['isMaxPage'] = false
    this.cookieService.setCookies('filterEmployee',  JSON.stringify(this.filterData))
    this.getListEmployee()
  }

  actionEmploye(key: string, item: any = {}): void {
    if (key === 'Add') {
      this.navigateUrl('/employee/new')
    } else if (key === 'Edit' ) {
      Swal.fire({
        icon: 'warning',
        title: `Do you want to edit ${item.username}?`,
        showCancelButton: true,
        confirmButtonText: "Yes",
        confirmButtonColor: "#FFC300",
        cancelButtonText: "No",
      }).then((result) => {
        /* Read more about isConfirmed, isDenied below */
        if (result.isConfirmed) {
          this.navigateUrl(`/employee/edit/${item.id}`)
        }
      });
    } else if (key === 'Delete') {
      Swal.fire({
        icon: 'error',
        title: `Do you want to delete ${item.username}?`,
        showCancelButton: true,
        confirmButtonText: "Yes",
        confirmButtonColor: "#E94F4F",
        cancelButtonText: "No",
      }).then((result) => {
        /* Read more about isConfirmed, isDenied below */
        if (result.isConfirmed) {
          this.deleteEmployee(item.id)
        }
      });
    } else if (key === 'View') {
      this.navigateUrl(`/employee/view/${item.id}`)
    }

  }

  async getListEmployee(): Promise<void> {
    const tmpQuery = [
      {
        param: 'page',
        value: this.filterData.currentPage
      },
      {
        param: 'limit',
        value: this.filterData.currentLimit
      },
      {
        param: 'orderby',
        value: this.filterData.orderby
      },
      {
        param: 'order',
        value: this.filterData.order
      },
      {
        param: 'search',
        value: this.filterData.inputSearch
      }
    ]
    const paramQuery = tmpQuery.map(v => v.param + '=' + v.value).join('&')

    const result = await this.employeeService.get(paramQuery)
    if (result.length < 1) {
      this.filterData['currentPage'] = this.filterData.previousPage
      this.filterData['isMaxPage'] = true
      this.getListEmployee()
      return
    }
    this.listEmployee = result
    this.filterData['previousPage'] = this.filterData.currentPage
  }

  async deleteEmployee(id: string): Promise<void> {

    const result = await this.employeeService.delete(id)
    if (!result) {
      return
    }
    Swal.fire({
      title: "Deleted!",
      text: `${result.username} has been deleted.`,
      icon: "success"
    })
    this.getListEmployee()
  }

}
