export class Dashboard {
  RefOfficeCode: string
  RefOfficeName: string
  Total: number
  constructor(data: any = {}) {
    this.RefOfficeCode = data.RefOfficeCode
    this.RefOfficeName = data.RefOfficeName
    this.Total = data.Total
  }
}
