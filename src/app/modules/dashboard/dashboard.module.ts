import { NgModule } from '@angular/core'

import { DashboardComponent } from './dashboard.component'
import { DashboardRoutingModule } from './dashboard-routing.module'
import { DashboardService } from './dashboard.service'
import { CookiesService } from 'src/app/service/cookies.service'
import { FormsModule } from '@angular/forms'
import { EmployeeService } from '../employee/employee.service'
import { CommonModule } from '@angular/common'

@NgModule({
  imports: [DashboardRoutingModule, FormsModule, CommonModule],
  declarations: [DashboardComponent],
  providers: [DashboardService, EmployeeService, CookiesService]
})
export class DashboardModule {}
