import { Injectable } from '@angular/core'
import { HttpService } from 'src/app/service/http.service'

@Injectable()
export class DashboardService {
  module: string = 'dashboard'
  constructor(private service: HttpService) {}
  public async get(paramQuery: string): Promise<any> {
    return await this.service.get(`/${this.module}?${paramQuery}`)
  }
}
