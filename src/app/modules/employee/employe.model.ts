export class Employee {
  username: string
  email: string
  firstName: string
  lastName: string
  birthDate: Date
  basicSalary: number
  status: string
  group: string
  description: Date
  id: number
  constructor(data: any = {}) {
    this.username = data.username
    this.email = data.email
    this.birthDate = data.birthDate
    this.description = data.description
    this.status = data.status
    this.group = data.group
    this.basicSalary = data.basicSalary
    this.firstName = data.firstName
    this.lastName = data.lastName
    this.id = data.id
  }
}
