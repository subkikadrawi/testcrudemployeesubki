import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { EmployeeComponent } from './employee.component';


const routes: Routes = [
  {
    path: 'new',
    component: EmployeeComponent,
    data: {
      title: 'Add'
    }
  },
  {
    path: 'edit/:id',
    component: EmployeeComponent,
    data: {
      title: 'Edit'
    }
  },
  {
    path: 'view/:id',
    component: EmployeeComponent,
    data: {
      title: 'View'
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class EmployeeRoutingModule {}
