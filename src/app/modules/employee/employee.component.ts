import { Component, OnInit } from '@angular/core'
import { CookiesService } from 'src/app/service/cookies.service'
import { ActivatedRoute, Router } from '@angular/router'
import { EmployeeService } from './employee.service'
import * as moment from 'moment';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import Swal from 'sweetalert2';
import { NgSelectConfig } from '@ng-select/ng-select';

@Component({
  templateUrl: 'employee.component.html'
})
export class EmployeeComponent implements OnInit {
  titlePage:string = ''
  employeeId:string = ''
  employee:any = {}
  form: FormGroup
  inValidateEmail: boolean = false
  groupOptions: any = [
    { id: 'Developer', name: 'Developer' },
    { id: 'Ui/Ux', name: 'Ui/Ux' },
    { id: 'Frontend', name: 'Frontend' },
    { id: 'Backend', name: 'Backend' },
    { id: 'Fullstack', name: 'Fullstack' },
    { id: 'Mobile', name: 'Mobile' },
    { id: 'Devops', name: 'Devops' },
    { id: 'Network', name: 'Network' },
    { id: 'ItSupport', name: 'ItSupport' },
    { id: 'Analyst', name: 'Analyst' },
  ];

  constructor(
    private fb: FormBuilder,
    private employeeService: EmployeeService,
    private cookieService: CookiesService,
    private router: Router,
    private route: ActivatedRoute,
    private config: NgSelectConfig
  ) {
    this.titlePage = this.route.snapshot.data['title']
    this.employeeId = this.route.snapshot.params['id']

    this.form = this.fb.group({
      // Define your form controls with validators if needed
      firstName: ['', Validators.required],
      lastName: ['', Validators.required],
      username: ['', Validators.required],
      email: ['', Validators.required],
      birthDate: ['', Validators.required],
      basicSalary: ['', Validators.required],
      status: ['', Validators.required],
      group: ['', Validators.required],
      description: ['', Validators.required],
    });


    this.config.notFoundText = 'Custom not found';
    this.config.appendTo = 'body';
    this.config.bindValue = 'value';
  }

  ngOnInit() {
    this.form.reset();
    if(this.employeeId){
      this.getEmployee()
    }
  }

  navigateUrl(directTo: string): void {
    this.router.navigate([directTo])
  }

  handleCancel(): void {
    this.navigateUrl('/dashboard');
  }

  isString(value: string | number): boolean {
    return typeof value === 'string';
  }

  formatCurrencyStringWithX(number: string): string {
    var numString = number;
    if (!this.isString(numString)) {
      numString = numString.toString()
    }
    const formattedString = 'Rp. ' + numString.replace(/\d/g, 'x').replace(/(xx)(?=(xxx)+(?!\w))/g, '$1.');
    return formattedString;
  }

  formatDate(date: string): string {
    return moment(date).format('DD-MMM-YYYY')
  }

  formatDateTime(date: string): string {
    return moment(date).format('DD-MMM-YYYY HH:mm:ss')
  }

  isValidEmail(event: any): boolean {
    // Regular expression for basic email format validation
    const emailRegex = /^[^\s@]+@[^\s@]+\.[^\s@]+$/
    this.inValidateEmail = !emailRegex.test(event.target.value)
    return emailRegex.test(event.target.value)
  }

  async getEmployee(): Promise<void> {

    const result = await this.employeeService.getbyId(this.employeeId)
    if (!result) {
      return
    }
    this.employee = result
    this.form.setValue({
      basicSalary: this.employee.basicSalary,
      birthDate: moment(this.employee.birthDate).format('YYYY-MM-DD'),
      description: moment(this.employee.description).format('YYYY-MM-DD'),
      email: this.employee.email,
      firstName: this.employee.firstName,
      group: this.employee.group,
      lastName: this.employee.lastName,
      status: this.employee.status,
      username: this.employee.username
    })

  }


  async onSubmit() {
    // Your form submission logic goes here
    if (this.form.valid) {
      // Form is valid, proceed with submission

      // Parse the date string
      const birthDate = moment.utc(this.form.value.birthDate);
      const description = moment.utc(this.form.value.description);
      const formattedBirthDateDate = birthDate.utc().format('YYYY-MM-DDTHH:mm:ss[Z]');
      const formattedDescriptionDate = description.utc().format('YYYY-MM-DDTHH:mm:ss[Z]');
      var dataPost = {
        basicSalary: this.form.value.basicSalary,
        birthDate: formattedBirthDateDate,
        description: formattedDescriptionDate,
        email: this.form.value.email,
        firstName: this.form.value.firstName,
        group: this.form.value.group,
        lastName: this.form.value.lastName,
        status: this.form.value.status,
        username: this.form.value.username
      }

      if (this.employeeId) {
        const result = await this.employeeService.update(dataPost, this.employeeId)
        Swal.fire({
          title: "Updated!",
          text: `${dataPost.username} has been Updated.`,
          icon: "success"
        })

      }else{
        const result = await this.employeeService.insert(dataPost)
        Swal.fire({
          title: "Created!",
          text: `${dataPost.username} has been Created.`,
          icon: "success"
        })
      }
      this.navigateUrl('/dashboard');

    } else {
      console.log('Form is invalid. Please check the fields.');
      this.form.markAllAsTouched()
    }
  }

}
