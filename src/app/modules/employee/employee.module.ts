import { NgModule } from '@angular/core'

import { CookiesService } from 'src/app/service/cookies.service'
import { EmployeeService } from './employee.service'
import { EmployeeComponent } from './employee.component'
import { EmployeeRoutingModule } from './employee-routing.module'
import { CommonModule } from '@angular/common'
import { FormsModule, ReactiveFormsModule } from '@angular/forms'
import { NgSelectModule } from '@ng-select/ng-select'

@NgModule({
  imports: [
    EmployeeRoutingModule,
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    NgSelectModule
  ],
  declarations: [EmployeeComponent],
  providers: [EmployeeService, CookiesService]
})
export class EmployeeModule {}
