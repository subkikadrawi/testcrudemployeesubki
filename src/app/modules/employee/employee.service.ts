import { Injectable } from '@angular/core'
import { HttpService } from 'src/app/service/http.service'

@Injectable()
export class EmployeeService {
  module: string = 'employee'
  constructor(private service: HttpService) {}
  public async get(paramQuery: string): Promise<any> {
    return await this.service.get(`/${this.module}?${paramQuery}`)
  }

  public async getbyId(id: string): Promise<any> {
    return await this.service.get(`/${this.module}/${id}`)
  }

  public async insert(data: any): Promise<any> {
    return await this.service.post(`/${this.module}`, data)
  }

  public async update(data: any, id: any): Promise<any> {
    return await this.service.put(`/${this.module}/${id}`, data)
  }

  public async delete(id: any): Promise<any> {
    return await this.service.delete(`/${this.module}/${id}`)
  }
}
