import { Injectable } from '@angular/core'
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot } from '@angular/router'
import { CookiesService } from './cookies.service'

@Injectable({ providedIn: 'root' })
export class AuthGuardService implements CanActivate {
  constructor(private router: Router, private cookieService: CookiesService) {}
  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    const userSession = this.cookieService.getCookies('userSession')
    if (userSession) return true

    this.router.navigate(['/login'], { queryParams: { returnUrl: state.url } })
    this.cookieService.deleteAllCookies()
    return false
  }
}
