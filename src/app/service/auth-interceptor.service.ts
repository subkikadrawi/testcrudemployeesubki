import { HttpClient, HttpEvent, HttpHandler, HttpHeaders, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ToastService } from 'angular-toastify';
import { catchError, Observable, switchMap, throwError } from 'rxjs';

export const InterceptorSkipHeader = 'X-Skip-Interceptor';

@Injectable({
  providedIn: 'root'
})
export class AuthInterceptorService implements HttpInterceptor {
  dataLoginauth: any;
  retryRefreshToken: any = 0;

  constructor(
    public httpClient: HttpClient,
    private _toastService: ToastService
  ) { }

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    const self = this;
    let handleRequest: any = request;
    const dataLogin: any = localStorage.getItem('dataLogin');
    self.dataLoginauth = JSON.parse(dataLogin);
    const isLoggedIn = self.dataLoginauth?.access_token;
    if (isLoggedIn) {
      console.log("you'r login");
    }

    return next.handle(handleRequest).pipe(
      catchError((returnedError) => {
        let errorMessage = '';
        // console.log('returnedError.error', returnedError.error);
        errorMessage = returnedError.error;
        self._toastService.error(errorMessage);
        return throwError(() => (errorMessage));
      })
    );
  }
}
