import { Injectable } from '@angular/core'
import { CookieService } from 'ngx-cookie-service'

@Injectable({ providedIn: 'root' })
export class CookiesService {
  constructor(private cookieService: CookieService) {}

  setCookies(key: string, value: string) {
    const midnight = new Date()
    midnight.setHours(23, 59, 59, 0)
    this.cookieService.set(key, value)
  }

  getCookies(key: string) {
    return this.cookieService.get(key)
  }

  deleteAllCookies() {
    this.cookieService.deleteAll()
  }

  deleteCookies(key: string) {
    this.cookieService.delete(key)
  }
}
