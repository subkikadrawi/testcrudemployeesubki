import { Injectable } from '@angular/core'
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http'

import { UserSession } from '../modules/auth/login/login.model'
import { CookiesService } from './cookies.service'
import { ToastService } from 'angular-toastify'

@Injectable({ providedIn: 'root' })
export class HttpService {
  private baseUrl: string = 'https://64874887beba6297279063f0.mockapi.io/api/v1'
  constructor(
    private http: HttpClient,
    private cookieService: CookiesService,
    private _toastService: ToastService) {
  }

  public async get(url: string, useToken: boolean = true): Promise<any> {
    try {
      const result = await this.http.get(this.baseUrl + url, this.baseHeader(useToken)).toPromise()
      return result
    } catch (err) {
      // return this.messageError(err)
    } finally {
    }
  }

  public async post(url: string, data: any, useToken: boolean = true): Promise<any> {
    try {
      const result = await this.http
        .post(this.baseUrl + url, JSON.stringify(data), this.baseHeader(useToken))
        .toPromise()
      return result
    } catch (err) {
      // return this.messageError(err)
    } finally {
    }
  }

  public async put(url: string, data: any, useToken: boolean = true): Promise<any> {
    try {
      const result = await this.http
        .put(this.baseUrl + url, JSON.stringify(data), this.baseHeader(useToken))
        .toPromise()
      return result
    } catch (err) {
      // return this.messageError(err)
    } finally {
    }
  }

  public async delete(url: string, useToken: boolean = true): Promise<any> {
    try {
      const result = await this.http
        .delete(this.baseUrl + url, this.baseHeader(useToken))
        .toPromise()
      return result
    } catch (err) {
      // return this.messageError(err)
    } finally {
    }
  }

  private baseHeader(useToken: boolean): any {
    const paramHeaders = { 'Content-Type': 'application/json' }
    if (useToken) {
    }
    return { headers: new HttpHeaders(paramHeaders) }
  }

  // public async messageError(error: Response | any): Promise<any> {
  //   let errMsg = 'Terjadi kesalahan, silahkan hubungi admin !'
  //   try {
  //     if (error instanceof HttpErrorResponse) {
  //       const meta = error.error.meta
  //       if (meta?.message != null) {
  //         errMsg = meta.message
  //       } else {
  //         if (error.status === 404) {
  //           errMsg = 'API Url tidak ditemukan !'
  //         } else {
  //         }
  //       }
  //     }
  //     return this._toastService.error(errMsg)
  //   } catch (err) {
  //     console.log(err)
  //     return this._toastService.error(errMsg)
  //   }
  // }

}
